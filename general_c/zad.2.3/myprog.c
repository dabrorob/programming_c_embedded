#include <stdio.h>

int main(void)
{
	char zc = 'A'; /* rozmiar 1 byte  -128 do 127 */
	/* unsigned char  0 do 255 */
	short int zs = 10000;
	/* unsigned short [int]*/
	int zi = 2000000000;
	long zl = 5000000000000;
	/* long int */
	long long zll = 0x0011223344556677;

	/* zmiennoprzecinkowe */
	float zf        = 0.0000000001234567890123;
	double zd       = 0.0000000001234567890123456789;
	long double zld = 0.000000000123456789012345678901234567890;

	printf("zc  = %10d - rozmiar: %ld\n", zc, sizeof(zc));
	printf("zs  = %10i - rozmiar: %ld\n", zs, sizeof(zs));
	printf("zi  = %10i - rozmiar: %ld\n", zi, sizeof(zi));
	printf("zl  = %10ld - rozmiar: %ld\n",zl, sizeof(zl));
	printf("zll = %10lld - rozmiar: %ld\n", zll, sizeof(zll));

	printf("\n zmiennoprzecinkowe \n\n");
	
	printf("zf  = %.20f rozmiar: %ld\n", zf, sizeof(zf));
	printf("zd  = %.30f rozmiar: %ld\n", zd, sizeof(zd));
	printf("zld = %.40Lf rozmiar: %ld\n", zld, sizeof(zld));
	
	return 0;
}
