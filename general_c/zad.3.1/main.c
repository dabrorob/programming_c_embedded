#include <stdio.h>
#include <string.h>


// przykladowe wywolanie 
// char b=3;
// set_bit(5,&b);
//

#define BN 5

void set_bit(unsigned char bit, unsigned int *v)
{

	*v |= (1<<bit);
}

int check_bit(unsigned int bit, const unsigned int *v)
{
	return *v & (1<<bit);
}	

void int2bin(int x)
{
	int i;

	for (i=(sizeof(x)*8)-1; i > -1 ; i--) {
		if(check_bit(i,&x))
			printf("1");
		else
			printf("0");	
	}
}

void int2binbuff(int x, char *buff, int size)
{
	int i;
	int j = 0;

	for (i=(sizeof(x)*8)-1; i > -1; i--) {
		buff[j++] = check_bit(i,&x) ? '1':'0';
		if(j>=size) break;	
	}
}



int main()
{
	int b = 3;
	char tab[33] = {0};
	char tab2[27] = {0};


	printf("b = 0x%02x\n", b);

	set_bit(BN,&b);

	printf("ustawiam bit %d\n", BN);
	printf("b = 0x%02x\n", b);


	printf("sprawdzam bit %d\n", 1);
	printf("bit %d jest %s\n", 1, check_bit(1,&b)?"ustawiony":"skasowany");




	int2bin(b);
	printf("\n");

	printf("zapisujemy bity do tab\n");
	int2binbuff(b,tab,32);
	printf("bity z tablicy: %s\n", tab);
	
	//memset(tab,0,33);

	int2binbuff(b,tab2,32);
	printf("bity z tablicy (blad 32 zamiast 26): %s\n", tab2);


	return 0;
}
