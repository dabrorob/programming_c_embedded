# include <stdio.h>

void show_bytes(void *w, int size)
{
	int i;

	for(i = 0; i < size; i++)
		printf("0x%02x ", *((unsigned char *)(w+i)) );
	printf("\n");
}


int main(void)
{
	int i;
	
	int x = 0x01020304;
	double d = 1.5;
	int *wsk = &x;
	void *w = &x;

	printf("zmienna x ma wartosc %d i rozmiar %ld\n", x, sizeof(x));
	printf("a w postaci szesnastkowej 0x%08x\n", x);

	printf("zmienna pod wskaznikiem wsk ma wartosc %d\n", *wsk);
	printf("adres zmiennej x zapisany w zmiennej wsk ma wartosc %p\n", wsk);

	
	printf("zmienna x bajt po bajcie: ");
	show_bytes(w,sizeof(x));

	w =(void *)&d;
	
	printf("zmienna d bajt po bajcie: ");
	show_bytes(w,sizeof(d));


	return 0;
}

