

zad. 2.1
	Napisz program w którym zadeklarujesz zmienne dla wszystkich znanych Ci typów podstawowych
	nadasz im wartości początkowe (różne od 0) i je wyświetlisz.
	Napisz Makefile do tego zadania i go użyj 

zad. 2.2
	Napisz program rozszerzający funkcje programu 2.1 o prośbę(y) do użytkownika o wpisanie
	z konsoli wartości do zmiennych i ich wczytanie a następnie ponowne wyśwetlenie.

zad. 2.3
	Napisz program rozszerzający funkcje programu 2.1 o wyśwetlenie rozmiarów (w bajtach)
	każdej zmiennej.

zad. 2.4
	Napisz program wyśwetlający bajt po bajcie (szesnastkowo) każdą ze zmiennych z prog 2.1.
  
	
