#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define MAX_TEXT 20

struct element {
	int id;
	char name[MAX_TEXT+1];
	struct element *next;
};

struct element *list;

int dodaj(struct element **pl, int id, char *name)
{
	struct element *we = (struct element *) malloc(sizeof(struct element));
	if(we == NULL)
		return(-ENOMEM);
	
	we->id = id;
	memcpy(we->name, name, (strlen(name) > MAX_TEXT)?MAX_TEXT:strlen(name));
	
	we->next = *pl;
	*pl = we;

	return 0;
}	

void wyswietl(struct element *pl) {

	while (pl != NULL) {
		printf("id: %2d, name: %-20s, next: %p\n", pl->id, pl->name, pl->next);
		pl = pl->next;
	}
}

int main()
{
	dodaj(&list, 1, "Agnieszka");
	dodaj(&list, 2, "Beata");
	dodaj(&list, 3, "Celina");
	dodaj(&list, 4, "Dorotka");
	dodaj(&list, 5, "Elwira");

	wyswietl(list);

	return 0;
}

