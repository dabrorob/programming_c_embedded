
zad. 3.1
	Napisz funkcję ustawiającą podany bit w podanej zmiennej

zad. 3.2
	Napisz funkcję sprawdzającą czy podany bit jest ustawiony w podanej zmiennej

zad. 3.3
	Napisz funkcję zamieniajacą podaną zmienną (int) na ciąg znaków reprezentujący
	jej postać binarną.
	Funkcja powinna przyjmować zmienną oraz wskaźnik na bufor dla ciągu znaków(bitów).

	Przykładowe wywołanie funkcji
		char tab_bit[33];
		int x = 3 << 10;

		int2bin(x,tab_bit);

		printf("liczba %d to binarnie %s\n", x, tab_bit);

zad. 3.4
	Napisz funkcję kasującą podany bit z podan ej zmiennej

zad. 3.5
	Napisz funkcję zamieniającą wszystkie bity podanej zmiennej na przeciwne
