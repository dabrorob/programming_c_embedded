#include <stdio.h>

#define BEG 48
#define END 67

int main(void)
{
	int i;

	for(i=BEG; i<=END; i++) {
		printf("%d - 0x%x - '%c'\n", i, i, i);
	}
	return 0;
}
