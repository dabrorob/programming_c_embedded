#include <stdio.h>

int main(void)
{
	char zc = 'A'; /* rozmiar 1 byte  -128 do 127 */
	/* unsigned char  0 do 255 */
	short int zs = 10000;
	/* unsigned short [int]*/
	int zi = 2000000000;
	long zl = 5000000000000;
	/* long int */
	long long zll = 0x0011223344556677;

	/* zmiennoprzecinkowe */
	float zf = 3.14;
	double zd = 12345.54321;
	long double zld = 1234567.7654321;

	printf("zc =  %c - %d\n", zc, zc);
	printf("zs = %i %d\n", zs ,zs);
	printf("zi = %i\n",zi);
	printf("zl = %ld\n",zl);
	printf("zll = %lld hex: 0x%llx\n", zll, zll);

	printf("zf = %f\n", zf);
	printf("zd = %f\n", zd);
	printf("zld = %Lf\n", zld);
	
	return 0;
}
