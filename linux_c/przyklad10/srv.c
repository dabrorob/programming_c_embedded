#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#define PORT_A  6000

int main()
{
	int gniazdo, gniazdo2;
	struct sockaddr_in adres;
	struct sockaddr_in adres_zew;
	char buf[1024];
	int nbytes, addrlen;
	int yes = 1;


	if((gniazdo = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Blad gniazda");
		exit(1);
	}

	adres.sin_family = AF_INET;
	adres.sin_port = htons(PORT_A);
	adres.sin_addr.s_addr=INADDR_ANY;
	memset(&(adres.sin_zero),0,8);



	setsockopt(gniazdo, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

	if(bind(gniazdo,(struct sockaddr*)&adres, sizeof(adres)) == -1) {
		perror("Blad bind");
		close(gniazdo);
		exit(1);
	}
	
	if( listen(gniazdo, 10) == -1){
		perror("Blad listen");
		close(gniazdo);
		exit(1);
	}
	
	printf("Serwer dziala\naby sprawdzić w drugiej konsoli wpisz polecenie:\n");
	printf("    telent localhost %d\n", PORT_A);
	printf("a następnie wpisz tam dowolny ciag znakow i zatwierdz eneterm\n");
	
	addrlen = sizeof(struct sockaddr_in);
	gniazdo2 = accept(gniazdo, (struct sockaddr *)&adres_zew, &addrlen);

	printf("Polaczenie klienta z adresu %s\n", inet_ntoa(adres_zew.sin_addr));

	if(gniazdo2 == -1) {
		perror("Blad accept");
		close(gniazdo);
		exit(1);
	}

	if (nbytes = recv(gniazdo2, buf, sizeof(buf), 0) <= 0 ) {
		perror("Blad recv");
       		close(gniazdo);
		close(gniazdo2);
	 	exit(1);
	}
	
	printf("Komunikat od klienta: %s\n", buf);

	if (send(gniazdo2, buf, strlen(buf), 0) == -1) {
		perror("Blad send");
       		close(gniazdo);
		close(gniazdo2);
	 	exit(1);
	}

	close(gniazdo2);
	close(gniazdo);

	return 0;
}

