 
#include <linux/init.h>           // Macros used to mark up functions e.g. __init __exit
#include <linux/module.h>         // Core header for loading LKMs into the kernel
#include <linux/kernel.h>         // Contains types, macros, functions for the kernel
 
MODULE_LICENSE("GPL");            ///< The license type -- this affects available functionality
MODULE_AUTHOR("Author Name");    ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("A simple Linux char driver for");  ///< The description -- see modinfo
MODULE_VERSION("0.1");            ///< A version number to inform users
 
static int __init hello_init(void){
   printk(KERN_INFO "Hello from kernel module\n");
   return 0;
}
 
static void __exit hello_exit(void){
   printk(KERN_INFO "Hello kernel module exiting\n");
}
 
module_init(hello_init);
module_exit(hello_exit);

